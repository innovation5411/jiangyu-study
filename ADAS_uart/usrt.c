#include "uart.h"

int open_port(char *com)
{
	int fd;
	// fd = open(com, O_RDWR|O_NOCTTY|O_NDELAY);
	fd = open(com, O_RDWR | O_NOCTTY); // 打开设备结点，O_NOCTTY

	if (-1 == fd)
	{
		return (-1);
	}

	if (fcntl(fd, F_SETFL, 0) < 0) /* 设置串口为阻塞状态*/
								   /*
								   1.fcntl(fd,F_SETEL,FNDELAY);
									读数据时不等待，没有数据就返回0；
								   2.fcntl(fd,F_SETFL,0);
									读数据时，没有数据阻塞
								   */
	{
		printf("fcntl failed!\n");
		return -1;
	}

	return fd;
}

int set_opt(int fd, int nSpeed, int nBits, char nEvent, int nStop)
{
	struct termios newtio, oldtio;

	if (tcgetattr(fd, &oldtio) != 0)
	{ // 获得驱动程序的默认参数放到oldtio中，备份下来
		perror("SetupSerial 1");
		return -1;
	}
	// 设置新的参数
	bzero(&newtio, sizeof(newtio)); // 清0，下面设置新的参数
	newtio.c_cflag |= CLOCAL | CREAD;
	newtio.c_cflag &= ~CSIZE;

	newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); /*Input*/
	newtio.c_oflag &= ~OPOST;						   /*Output*/

	switch (nBits) // 设置数据位的个数
	{
	case 7:
		newtio.c_cflag |= CS7;
		break;
	case 8:
		newtio.c_cflag |= CS8;
		break;
	}

	switch (nEvent) // 设置校验位
	{
	case 'O':
		newtio.c_cflag |= PARENB;
		newtio.c_cflag |= PARODD;
		newtio.c_iflag |= (INPCK | ISTRIP);
		break;
	case 'E':
		newtio.c_iflag |= (INPCK | ISTRIP);
		newtio.c_cflag |= PARENB;
		newtio.c_cflag &= ~PARODD;
		break;
	case 'N':
		newtio.c_cflag &= ~PARENB;
		newtio.c_iflag &= ~INPCK;    
		break;
	}

	switch (nSpeed) // 设置波特率
	{
	case 2400:
		cfsetispeed(&newtio, B2400);
		cfsetospeed(&newtio, B2400);
		break;
	case 4800:
		cfsetispeed(&newtio, B4800);
		cfsetospeed(&newtio, B4800);
		break;
	case 9600:
		cfsetispeed(&newtio, B9600);
		cfsetospeed(&newtio, B9600);
		break;
	case 115200:
		cfsetispeed(&newtio, B115200);
		cfsetospeed(&newtio, B115200);
		break;
	case 19200:
		cfsetispeed(&newtio, B19200);
		cfsetospeed(&newtio, B19200);
		break;
	default:
		cfsetispeed(&newtio, B9600);
		cfsetospeed(&newtio, B9600);
		break;
	}

	if (nStop == 1) // 设置停止位
		newtio.c_cflag &= ~CSTOPB;
	else if (nStop == 2)
		newtio.c_cflag |= CSTOPB;

	newtio.c_cc[VMIN] = 1;	/* 读数据时的最小字节数: 没读到这些数据我就不返回! */
	newtio.c_cc[VTIME] = 1; /* 等待第1个数据的时间:
							 * 比如VMIN设为10表示至少读到10个数据才返回,
							 * 但是没有数据总不能一直等吧? 可以设置VTIME(单位是10秒)
							 * 假设VTIME=1，表示:
							 *    10秒内一个数据都没有的话就返回
							 *    如果10秒内至少读到了1个字节，那就继续等待，完全读到VMIN个数据再返回
							 */

	tcflush(fd, TCIFLUSH);
	//newtio.c_cflag &= ~CRTSCTS;

	if ((tcsetattr(fd, TCSANOW, &newtio)) != 0) // tcsetattr函数设置行规程
	{
		perror("com set error");
		return -1;
	}
	// printf("set done!\n");
	return 0;
}

void send_string(char *str, int fd)
{
	int byte_sent = 0;
	while (byte_sent < strlen(str))
	{
		int len = write(fd, str + byte_sent, 0);
	}
}

static void write_value(unsigned int *array, int index, unsigned int value)
{
	for (int i = 0; i <= TIMES; i++)
	{
		if (i == index)
		{
			array[i] = value;
			break;
		}
	}
}

void buffer_init(unsigned int *str, char *argv1, char *argv2)
{
	int ch;
	unsigned int data;
	u_int8_t symbol;
	ch = *argv1;
	switch (ch)
	{
	case 'a':
		symbol = 1;
		break;
	case 'b':
		symbol = 2;
		break;
	case 'c':
		symbol = 3;
		break;
	case 'd':
		symbol = 4;
		break;
	case 'e':
		symbol = 5;
		break;
	case 'f':
		symbol = 6;
		break;
	case 'g':
		symbol = 7;
		break;
	case 'h':
		symbol = 8;
		break;
	case 'i':
		symbol = 9;
		break;
	case 'j':
		symbol = 10;
		break;
	case 'k':
		symbol = 11;
		break;
	default:
		break;
	}
	// data=*argv2;
	// sprintf(argv2,"0x%x",data);
	sscanf(argv2, "%x", &data);
	if (symbol == 0x51 && data == 0x01)
	{
		printf("model is LDW and DATA=1\n");
	}

	write_value(str, 0, 0xFF);
	write_value(str, 1, 0xBB);
	write_value(str, 2, 0x01);
	write_value(str, 3, 0x02);
	write_value(str, 4, 0x00);
	write_value(str, 5, 0x00);
	write_value(str, 6, 0xCC);
}
