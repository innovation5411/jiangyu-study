#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <error.h>
#include <sys/stat.h>
#include <termios.h>
#include <sys/types.h>

#define TIMES   7
#define DATA    0x51 



int open_port(char *com);
int set_opt(int fd,int nSpeed, int nBits, char nEvent, int nStop);
void send_string(char* str,int fd);
void buffer_init(unsigned int *str,char *argv1,char *argv2);
static void wirte_value(u_int8_t *array,int index,u_int8_t value);