#include "uart.h"
#include <stdio.h>

// static unsigned int *read_buffer;
// static unsigned int *buffer;

//发送16进制数指令数组
static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x00,0x00,0xcc};//DMS闭眼
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x03,0x03,0xcc};//DMS打哈欠
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x06,0x06,0xcc};//DMS分心驾驶
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x01,0x01,0xcc};//DMS抽烟
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x02,0x02,0xcc};//DMS打电话
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x07,0x07,0xcc};//DMS喝水
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x09,0x09,0xcc};//DMS镜头遮挡
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x04,0x04,0xcc};//DMS墨镜检测
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x05,0x05,0xcc};//DMS未带口罩
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x08,0x08,0xcc};//DMS未系安全带
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x0A,0x0A,0xcc};//DMS前车启动
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x0c,0x0c,0xcc};//DMS前车碰撞
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x0b,0x0b,0xcc};//DMS车距过近
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x0d,0x0d,0xcc};//DMS行人碰撞
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x0e,0x0e,0xcc};//DMS车道左偏移
//static unsigned char buffer[]={0xff,0xbb,0x01,0x02,0x0e,0x0e,0xcc};//DMS车道右偏移


//static unsigned char buffer[]={0xff,0xbb,0x33,0x20,0x40,0x0a,0xcc};//DMS声音指令


static unsigned char *read_buffer;


int main(int argc, char **argv)
{
	int fd;
	int iRet;
	int lenth;

	if (argc != 2)
	{

		printf("Usage: \n");
		printf("%s </dev/ttySAC1 or other>\n", argv[0]);
		return -1;
	}

	fd = open_port(argv[1]);
	if (fd < 0)
	{
		printf("open %s err!\n", argv[1]);
		return -1;
	}

	iRet = set_opt(fd, 19200, 8, 'N', 1); // 这就是我要重点关注的，设置比特率位115200，数据位为8，没有校验位，停止位的个数是1。
	if (iRet)
	{
		printf("set port err!\n");
		return -1;
	}
	//char bufx[sizeof(buffer) / 2];
	// printf("Enter a char: ");
	// random_num(buffer,TIMES);
	while (1)
	{
		// unsigned int *hex_buffer;
		// buffer=(unsigned int *)malloc(sizeof(unsigned int)*TIMES);
		read_buffer = (unsigned char *)malloc(sizeof(unsigned char) * TIMES);
		// buffer_init(buffer,argv[2],argv[3]);
		// sscanf(buffer,"%x",hex_buffer);
		// printf("return = %d\n", HexToAscill(buffer, bufx, sizeof(bufx)));
		lenth = write(fd, buffer,sizeof(buffer)); // 发给指定串口数据
		if (lenth != 0)							   // 在命令行上打印出来
		{
			for (int i = 0; i < TIMES; i++)
				printf("The write number is:0x%X\n", buffer[i]);
			printf("%d\n", lenth);
		}
		else
			printf("can not write data\n");
		lenth = 0;

		lenth = read(fd, read_buffer,TIMES);
		if (lenth != 0) // 在命令行上打印出来
		{
			for (int i = 0; i < TIMES; i++)
				printf("The read number is:0x%X\n", read_buffer[i]);
			printf("%d\n", lenth);
		}
		else
			printf("can not read data\n");
		// free(buffer);
		free(read_buffer);
		sleep(2);
	}
	return 0;
}
